package com.example.desafio_cs.test;

import java.sql.SQLException;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.widget.Button;
import android.widget.EditText;

import com.example.desafio_cs.R;
import com.example.desafio_cs.entidades.Endereco;
import com.example.desafio_cs.view.CEPActivity_;

public class TestCEPActivity extends ActivityUnitTestCase<CEPActivity_>{ 

	private CEPActivity_ activity;
	
	public TestCEPActivity() {
		super(CEPActivity_.class);
	}
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		Intent intent = new Intent(getInstrumentation().getContext(), CEPActivity_.class);
		startActivity(intent, null, null);
		activity = getActivity();
	}

	public void testInserirMascaraCEP(){
		
		EditText campoCEP = (EditText) activity.findViewById(R.id.campoCEP);
		String CEP = "66075104";
		StringBuilder builder = new StringBuilder();
		for(Character caracter : CEP.toCharArray()){
			
			builder.append(caracter);
			campoCEP.setText(builder.toString());
		}
		
		assertTrue(campoCEP.getText().toString().length() == 9);
		assertTrue(campoCEP.getText().toString().equalsIgnoreCase("66075-104"));
		
	}
	
	public void testObterEnderecoViaJSON(){
		
		EditText campoCEP = (EditText) activity.findViewById(R.id.campoCEP);
		String CEP = "66075104";
		campoCEP.setText(CEP);
		
		Button botaoPesquisar = (Button) activity.findViewById(R.id.botaoPesquisar);
		botaoPesquisar.performClick();
		
		Endereco endereco = activity.getEndereco();
		
		assertNotNull(endereco);
		assertNotNull(endereco.getCidade());
	}

	
	public void testPersistirEndereco() throws SQLException{
		
		EditText campoCEP = (EditText) activity.findViewById(R.id.campoCEP);
		String CEP = "66075104";
		campoCEP.setText(CEP);
		
		Button botaoPesquisar = (Button) activity.findViewById(R.id.botaoPesquisar);
		botaoPesquisar.performClick();
		
		Endereco endereco = activity.getEndereco();
		
		activity.getEnderecoDAO().createIfNotExists(endereco);
		
		assertNotNull(activity.getEnderecoDAO().queryForId(endereco.getCep()));
		
	}
	
	
}
