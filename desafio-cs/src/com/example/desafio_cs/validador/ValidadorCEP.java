package com.example.desafio_cs.validador;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import android.widget.EditText;

import com.example.desafio_cs.R;
import com.example.desafio_cs.excessoes.CEPInvalidoException;
import com.example.desafio_cs.excessoes.ValidacaoException;
import com.example.desafio_cs.mascaras.MascaraCEP;
import com.example.desafio_cs.view.CEPActivity;

@EBean
public class ValidadorCEP implements Validador {
	
	@RootContext
	CEPActivity activity;
	
	private String cep;
	
	@Override
	public void validar() throws ValidacaoException {
		EditText editText = (EditText) activity.findViewById(R.id.campoCEP);
		cep =  MascaraCEP.removerMascara(editText.getText().toString()); 
		validarComprimentoCEP(cep);
		validarConteudoCEP(cep);
	}
	
	private void validarComprimentoCEP(final String cep) throws CEPInvalidoException{
		if(cep.length() != 8){
			throw new CEPInvalidoException();
		}
	}
	
	private void validarConteudoCEP(final String cep) throws CEPInvalidoException{
		try {
			Integer.parseInt(cep);
		} catch (Exception e) {
			throw new CEPInvalidoException(); 
		}
	}


	
	

}
