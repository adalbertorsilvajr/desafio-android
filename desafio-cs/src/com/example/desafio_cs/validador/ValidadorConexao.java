package com.example.desafio_cs.validador;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.desafio_cs.excessoes.ConexaoInativaException;
import com.example.desafio_cs.excessoes.ValidacaoException;
import com.example.desafio_cs.view.CEPActivity;

@EBean
public class ValidadorConexao implements Validador{
	
	@RootContext
	CEPActivity activity;

	@Override
	public void validar() throws ValidacaoException {
		isConexaoAtiva();
//		efetuarTesteDeConectividade();
		
	}
	
	private void isConexaoAtiva() throws ConexaoInativaException{
		ConnectivityManager connectivityManager  = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifi = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		NetworkInfo mobile = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
		
		if(!wifi.isConnectedOrConnecting() && !mobile.isConnectedOrConnecting()){
			throw new ConexaoInativaException();	        
		}
	}
	
//	private void efetuarTesteDeConectividade() throws ConexaoInativaException{
//		 try {
//	            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
//	            urlc.setRequestProperty("User-Agent", "Test");
//	            urlc.setRequestProperty("Connection", "close");
//	            urlc.setConnectTimeout(1500); 
//	            urlc.connect();
//	        } catch (IOException e) {
//	        	throw new ConexaoInativaException();	        
//	        }
//	}
	
}
