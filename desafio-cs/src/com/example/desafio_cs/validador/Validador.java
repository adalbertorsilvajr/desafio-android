package com.example.desafio_cs.validador;

import com.example.desafio_cs.excessoes.ValidacaoException;

public interface Validador {
	
	public void validar() throws ValidacaoException;
	
}
