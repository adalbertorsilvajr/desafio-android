package com.example.desafio_cs.excessoes;

public class ConexaoInativaException extends ValidacaoException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1785103717540735068L;

	@Override
	public String getMessage() {
		return "Seu dispositivo não possui uma conexão ativa com a internet, por favor ativar";
	}

}
