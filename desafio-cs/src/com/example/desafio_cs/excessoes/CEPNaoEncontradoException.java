package com.example.desafio_cs.excessoes;

public class CEPNaoEncontradoException extends Exception{

	@Override
	public String getMessage() {
		return "CEP não encontrado !";
	}
	
}
