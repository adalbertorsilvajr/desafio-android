package com.example.desafio_cs.excessoes;

public abstract class ValidacaoException extends Exception {

	public abstract String getMessage();
	
}
