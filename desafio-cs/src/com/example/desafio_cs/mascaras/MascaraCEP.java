package com.example.desafio_cs.mascaras;

import android.widget.EditText;

public abstract class MascaraCEP {

	private static String PADRAO = "#####-###";
	private static boolean isAtualizando;
	
	public static String removerMascara(String texto) {
		return texto.replaceAll("[-]", "");
	}

	public static void aplicarMascara(EditText editText, CharSequence s){
		
		String valorAntigo = "";
		String valorSemMascara = MascaraCEP.removerMascara(s.toString());
		String mascara = "";
		if (isAtualizando) {
			valorAntigo = valorSemMascara;
			isAtualizando = false;
			return;
		}
		int i = 0;
		for (char m : PADRAO.toCharArray()) {
			if (m != '#' && valorSemMascara.length() > valorAntigo.length()) {
				mascara += m;
				continue;
			}
			try {
				mascara += valorSemMascara.charAt(i);
			} catch (Exception e) {
				break;
			}
			i++;
		}
		isAtualizando = true;
		editText.setText(mascara);
		editText.setSelection(mascara.length());
	}
	
}
