package com.example.desafio_cs.view;

import java.sql.SQLException;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.ViewById;

import android.content.pm.ActivityInfo;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.example.desafio_cs.R;
import com.example.desafio_cs.entidades.Endereco;

@EBean
public class Popup implements OnClickListener{
	
	PopupWindow popupListaCep;
	
	@RootContext
	CEPActivity activity;
	
	@ViewById
	Button botaoFecharPopup, botaoVisualizarPesquisas;
	
	ListView listaEnderecos;
	
	@Click(R.id.botaoFecharPopup)
	public void fecharPopup(){
		popupListaCep.dismiss();
	}
	
	public void exibir(){
		try {
			listaEnderecos.setAdapter(new ArrayAdapter<Endereco>(activity, android.R.layout.simple_list_item_1, activity.getEnderecoDAO().queryForAll()));
//			popupListaCep.showAsDropDown(botaoVisualizarPesquisas, 50, 30);
			
			popupListaCep.showAtLocation(botaoVisualizarPesquisas, Gravity.CENTER, 50, 30);
			
		} catch (SQLException e) {
			activity.registrarMensagemErro(e);
			Log.e("ERRO DE BANCO DE DADOS!", e.getMessage());
		}
	}
	
	@AfterInject
	public void carregarComponentesPopup(){
		
			LinearLayout popupLayout = (LinearLayout) activity.getLayoutInflater().inflate(R.layout.popup, null);
			popupListaCep = new PopupWindow(popupLayout, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			popupListaCep.setFocusable(true);
			listaEnderecos = (ListView) popupListaCep.getContentView().findViewById(R.id.listaEnderecos);
			botaoFecharPopup = (Button) popupLayout.findViewById(R.id.botaoFecharPopup);
			botaoFecharPopup.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v) {
		popupListaCep.dismiss();
	}

}
