package com.example.desafio_cs.view;

import java.sql.SQLException;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OrmLiteDao;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;
import org.androidannotations.api.BackgroundExecutor;
import org.springframework.web.client.HttpClientErrorException;

import android.app.Activity;
import android.app.AlertDialog;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.desafio_cs.R;
import com.example.desafio_cs.entidades.Endereco;
import com.example.desafio_cs.excessoes.CEPInvalidoException;
import com.example.desafio_cs.excessoes.CEPNaoEncontradoException;
import com.example.desafio_cs.excessoes.ValidacaoException;
import com.example.desafio_cs.helper.BancoDadosHelper;
import com.example.desafio_cs.mascaras.MascaraCEP;
import com.example.desafio_cs.rest.ClienteRestCEP;
import com.example.desafio_cs.validador.Validador;
import com.example.desafio_cs.validador.ValidadorCEP;
import com.example.desafio_cs.validador.ValidadorConexao;
import com.j256.ormlite.dao.Dao;


@EActivity(R.layout.activity_cep)
public class CEPActivity extends Activity{ 

	@ViewById
	EditText campoCEP;
	
	@ViewById
	Button botaoPesquisar, botaoVisualizarPesquisas;
	
	@Bean
	Popup popup;
	
	@Bean(ValidadorConexao.class)
	Validador validadorConexao;
	
	@Bean(ValidadorCEP.class)
	Validador validadorCEP;
	
	@RestService
	ClienteRestCEP clienteRestCEP;
	
	@OrmLiteDao(helper=BancoDadosHelper.class, model=Endereco.class)
	Dao<Endereco, String> enderecoDAO;
	
	private Endereco endereco;
	
	@TextChange(R.id.campoCEP)
	public void aplicarMascara(TextView textView, CharSequence s){
		MascaraCEP.aplicarMascara((EditText) textView, s);;
	}
	
	@Click(R.id.botaoPesquisar)
	public void pesquisarCEP(){
		executarPesquisa();
	}

	@Background
	public void executarPesquisa(){
		try {
			executarValidacoes();
			obterEnderecoViaREST();
			persistirEndereco();
			exibirResultadoPesquisa();
		} catch (Exception e) {
			registrarMensagemErro(e);
		}
		
		
	}
	
	public void executarValidacoes() throws ValidacaoException{
		validadorConexao.validar();
		validadorCEP.validar();
	}
	
	private void obterEnderecoViaREST() throws CEPNaoEncontradoException {
		try {
			endereco = clienteRestCEP.getEndereco(MascaraCEP.removerMascara(campoCEP.getText().toString()));
		} catch (Exception e) {
			throw new CEPNaoEncontradoException();
		}
	}
	
	private void persistirEndereco() {
		try {
			enderecoDAO.createIfNotExists(endereco);
		} catch (SQLException e) {
			Log.e("ERRO DO BANCO DE DADOS", e.getMessage(), e);
		}
	}
	
	@Click(R.id.botaoVisualizarPesquisas)
	public void visualizarPesquisas(){
		popup.exibir();
	}
	
	@UiThread
	public void registrarMensagemErro(Exception e){
		Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
	}
	
	@UiThread
	public void exibirResultadoPesquisa(){
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Resultado da Pesquisa");
		builder.setMessage(getEndereco().toString());
		builder.setPositiveButton("Fechar", null);
		builder.show();
	}
	
	public Endereco getEndereco(){
		return endereco;
	}
	
	public Dao<Endereco, String> getEnderecoDAO(){
		return enderecoDAO;
	}
}
