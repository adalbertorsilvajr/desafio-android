package com.example.desafio_cs.entidades;

import java.io.Serializable;

import org.androidannotations.annotations.EBean;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Endereco implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7970559993659295229L;
	
	@DatabaseField
	private String bairro;
	
	@DatabaseField(id=true)
	private String cep;
	
	@DatabaseField
	private String cidade;
	
	@DatabaseField
	private String estado;
	
	@DatabaseField
	private String logradouro;
	
	@DatabaseField
	private String tipoDeLogradouro;
	
	public Endereco() {}

	@JsonCreator
	public Endereco(@JsonProperty("bairro") String bairro, @JsonProperty("cep") String cep, 
					@JsonProperty("cidade") String cidade, @JsonProperty("estado") String estado,
					@JsonProperty("logradouro") String logradouro, @JsonProperty("tipodelogradouro") String tipoDeLogradouro) {
		super();
		this.bairro = bairro;
		this.cep = cep;
		this.cidade = cidade;
		this.estado = estado;
		this.logradouro = logradouro;
		this.tipoDeLogradouro = tipoDeLogradouro;
	}
	
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getTipoDeLogradouro() {
		return tipoDeLogradouro;
	}

	public void setTipoDeLogradouro(String tipoDeLogradouro) {
		this.tipoDeLogradouro = tipoDeLogradouro;
	}
	
	@Override
	public String toString() {
		
		StringBuilder builder = new StringBuilder();
		builder.append("CEP: ").append(cep).append("\n")
			   .append("Endereço: ").append(logradouro).append("\n")
			   .append("Bairro: ").append(bairro).append("\n")
			   .append("Cidade: ").append(cidade).append("\n")
			   .append("Estado: ").append(estado);
		return builder.toString();
	}

}
