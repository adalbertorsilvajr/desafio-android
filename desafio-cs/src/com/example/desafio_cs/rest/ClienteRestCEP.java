package com.example.desafio_cs.rest;

import org.androidannotations.annotations.rest.Accept;
import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Rest;
import org.androidannotations.api.rest.MediaType;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;

import com.example.desafio_cs.entidades.Endereco;

@Rest(rootUrl="http://correiosapi.apphb.com/cep", converters={MappingJacksonHttpMessageConverter.class})
public interface ClienteRestCEP{ 
	
	@Get("/{cep}")
	@Accept(MediaType.APPLICATION_JSON)
	public Endereco getEndereco(String cep);

}
